This project based on Christopher Herbert's ["Reconstructing Cave Story"](https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.youtube.com%2Fplaylist%3Flist%3DPL006xsVEsbKjSKBmLu1clo85yLrwjY67X&session_token=1f8IgNHMWKHO6UV96j9zL0OKMK58MTM5MDI5MTY1M0AxMzkwMjA1MjUz).

I decided to make the same with SDL 2.0 and C++11.

Used materials:
* [Lesson 5: Clipping Sprite Sheets](http://twinklebear.github.io/sdl2%20tutorials/2013/08/27/lesson-5-clipping-sprite-sheets/) by [Twinklebear](http://twinklebear.github.io/)
* [SDL 1.2 to 2.0 Migration Guide](http://wiki.libsdl.org/MigrationGuide)
